describe "POST /sessions" do
  context "login com sucesso" do
    before(:all) do
      payload = { email: "teste@teste.com.br", password: "1234" }
      @result = Sessions.new.login(payload)
    end

    it "validade status code" do
      expect(@result.code).to eql 200
    end

    it "validade tamanho id" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end
  examples = Helpers::get_fixture("login")

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end

      it "validade status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "mensagem não autorizado" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
