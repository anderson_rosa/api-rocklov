require_relative "routes/signup"
require_relative "routes/sessions"
require_relative "routes/equipos"
require_relative "libs/mongo"
require_relative "helpers"

require "logger"
require "digest/md5"

def logger
  return Logger.new(STDOUT)
end

logger.info("Carregando arquivo spec_helper de configuracao")

def to_md5(pass)
  return Digest::MD5.hexdigest(pass)
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
  config.shared_context_metadata_behavior = :apply_to_host_groups
  logger.info("Inserindo massas de login no banco de dados MongoDB")
  config.before(:suite) do
    users = [
      { name: "andso 1", email: "andso1@teste.com.br", password: to_md5("1234") },
      { name: "andso 2", email: "andso2@teste.com.br", password: to_md5("1234") },
      { name: "andso 3", email: "andso3@teste.com.br", password: to_md5("1234") },
      { name: "andso 4", email: "andso4@teste.com.br", password: to_md5("1234") },
      { name: "joão nome", email: "joao@gmail.com.br", password: to_md5("pwd123") },
      { name: "teste", email: "teste@teste.com.br", password: to_md5("1234") },

    ]
    MongoDB.new.drop_table
    MongoDB.new.insert_users(users)
  end
end
